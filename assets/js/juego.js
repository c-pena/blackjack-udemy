/**
* 2C = Tow of Clubs
* 2D = Two of Diaminds
* 2H = Two of Hearts
* 2S = Two of Spades
*/

let deck = [];
const cartaTipos = ['C', 'D', 'H', 'S'];
const cartaEspeciales = ['A', 'J', 'K', 'Q'];

const crearDeck = () => {    
    for( let i = 2; i <= 10; i++ ){
        for( let tipo of cartaTipos ){
            deck.push( i + tipo );
        }
    }

    for( let tipo of cartaTipos){
        for( let especial of cartaEspeciales){
            deck.push( especial + tipo);
        }
    }
    deck = _.shuffle( deck );
    console.log( deck );

    return deck;
}

crearDeck();

const pedirCarta = () => {
    if( deck.length === 0 ){
        throw 'no hay mas cartas!!'
    }
    const cartaSolicitada = deck.pop();

    console.log(deck, 'length: ' + deck.length);
    console.log(cartaSolicitada);

    return cartaSolicitada;
}

console.log('deck: ', deck);

// pedirCarta();

const valorCarta = ( carta ) => {
    const valorCarta = carta.substring(0, carta.length - 1 );
    console.log( valorCarta); 
}

valorCarta('JD');



